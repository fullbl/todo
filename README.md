# Simple TO DO app

This is a simple TO DO app built in vue.js and laravel.


Usage:

Download:
```
git clone git@gitlab.com:fullbl/todo.git
```
Install:
```
docker compose up -d
```

Enjoy:
```
http://localhost
```