<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('/register', 'App\Http\Controllers\UserController@register');
Route::post('/login', 'App\Http\Controllers\UserController@login');

Route::middleware('auth:sanctum')->prefix('to-dos')->group(function () {
    Route::get('/', 'App\Http\Controllers\ToDoController@index');
    Route::post('/', 'App\Http\Controllers\ToDoController@store');
    Route::put('/{toDo}', 'App\Http\Controllers\ToDoController@update');
    Route::delete('/{toDo}', 'App\Http\Controllers\ToDoController@destroy');
});