<?php

namespace App\Http\Controllers;

use App\Models\ToDo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ToDoController extends Controller
{
    public function index(Request $request)
    {
        $toDos = ToDo::where('user_id', $request->user()->id)->get();

        return response()->json($toDos, Response::HTTP_OK);
    }

    public function store(Request $request)
    {
        $data = $this->validate($request, [
            'title' => 'required|string|min:1|max:100',
            'description' => 'required|string|min:1|max:255',
            'category' => 'required|string|min:1|max:100',
            'status' => 'required|string|min:1|max:50',
        ]);

        $data['user_id'] = $request->user()->id;
        $toDo = ToDo::create($data);

        return response()->json($toDo, Response::HTTP_CREATED);
    }

    public function update(Request $request, ToDo $toDo)
    {
        if($toDo->user_id !== $request->user()->id) {
            return response()->json([
                'message' => 'This to-do does not belong to you.'
            ], Response::HTTP_FORBIDDEN);
        }

        $data = $this->validate($request, [
            'title' => 'required|string|min:1|max:100',
            'description' => 'required|string|min:1|max:255',
            'category' => 'required|string|min:1|max:100',
            'status' => 'required|string|min:1|max:50',
        ]);

        $toDo->update($data);

        return response()->json($toDo, Response::HTTP_ACCEPTED);
    }

    public function destroy(Request $request, ToDo $toDo)
    {
        if($toDo->user_id !== $request->user()->id) {
            return response()->json([
                'message' => 'This to-do does not belong to you.'
            ], Response::HTTP_FORBIDDEN);
        }
        $toDo->delete();

        return response()->json([], Response::HTTP_OK);
    }
}
