export default interface ToDo {
  id?: number
  title: string
  description: string
  category: string
  status: string
}
