import dataService from './DataService'

interface AuthService {
  register: (name: string, password: string) => Promise<boolean>
  login: (name: string, password: string) => Promise<boolean>
  logout: () => void
  isLoggedIn: () => boolean
}

const service: AuthService = {
  async register(name, password) {
    try {
      await dataService.post<{ token: string }>(import.meta.env.VITE_API_URL + '/register', {
        name,
        password
      })
      return true
    } catch (e) {
      return false
    }
  },

  async login(name, password) {
    try {
      const data = await dataService.post<{ token: string }>(
        import.meta.env.VITE_API_URL + '/login',
        { name, password }
      )
      if (data.token) {
        localStorage.setItem('token', data.token)
        return true
      } else {
        return false
      }
    } catch (e) {
      return false
    }
  },

  logout() {
    localStorage.removeItem('token')
  },

  isLoggedIn() {
    return null !== localStorage.getItem('token')
  }
}

export default service
