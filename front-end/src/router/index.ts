import { createRouter, createWebHistory } from 'vue-router'
import authService from '@/services/AuthService'
import ToDoPage from '../views/ToDoPage.vue'
import LoginPage from '../views/LoginPage.vue'
import RegisterPage from '../views/RegisterPage.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: ToDoPage
    },
    {
      path: '/login',
      name: 'login',
      component: LoginPage,
      meta: { auth: false }
    },
    {
      path: '/register',
      name: 'register',
      component: RegisterPage,
      meta: { auth: false }
    }
  ]
})

router.beforeEach(async (to) => {
  if (false !== to.meta.auth && !authService.isLoggedIn()) {
    return { name: 'login' }
  }
})

export default router
